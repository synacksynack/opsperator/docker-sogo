FROM opsperator/apache

# Sogo image for OpenShift Origin

ARG DO_UPGRADE=

LABEL io.k8s.description="SOGo 5 Webmail." \
      io.k8s.display-name="SOGo 5" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="sogo" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-sogo" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="5.7.1"

USER root

COPY config/* /

RUN set -x \
    && rm -rf /var/lib/apt/lists/* \
    && echo "# Install SOGo Dependencies" \
    && apt-get update \
    && apt-get -y install --no-install-recommends apt-transport-https wget \
	gnupg2 mariadb-client postgresql-client \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && mv /nsswrapper.sh /usr/local/bin/sogo-nsswrapper.sh \
    && mv /main.sh /usr/local/bin/main-init.sh \
    && echo "# Install SOGo" \
    && mkdir -p /usr/share/doc/sogo \
    && echo exit 0 >>/usr/share/doc/sogo/dummy.sh \
    && gpg --keyserver keyserver.ubuntu.com --recv CB2D3A2AA0030E2C \
    && gpg --export --armor CB2D3A2AA0030E2C | apt-key add - \
    && wget -O- "https://keys.openpgp.org/vks/v1/by-fingerprint/74FFC6D72B925A34B5D356BDF8A27B36A6E2EAE9" \
	| gpg --dearmor | apt-key add - \
    && mv /sogo.list /etc/apt/sources.list.d/ \
    && apt-get update \
    && apt-get install -y sogo sope4.9-gdl1-mysql sope4.9-gdl1-postgresql \
	sogo-activesync \
    && echo "# Enabling Proxy Modules" \
    && a2enmod proxy proxy_http proxy_wstunnel proxy_balancer ldap authnz_ldap \
    && a2dismod perl \
    && echo "# Fixing permissions" \
    && for d in /etc/sogo /run /var/spool/sogo /var/log/sogo; \
	do \
	    mkdir -p "$d"; \
	    chown -R g=u $d || echo nevermind; \
	    chown -R 1001:0 $d || echo nevermind; \
	done \
    && echo "# Cleaning Up" \
    && rm -vf /etc/apache2/*/*sogo* \
    && apt-get -y remove --purge apt-transport-https gnupg2 wget \
    && apt-get autoremove -y --purge \
    && apt-get clean \
    && mv /usr/share/doc/sogo /usr/src \
    && rm -rvf /etc/apache2/ports.conf /usr/share/man /usr/share/doc \
	/var/lib/apt/lists/* /root/.gnupg \
    && mkdir -p /usr/share/doc \
    && mv /usr/src/sogo /usr/share/doc/sogo \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

ENTRYPOINT [ "dumb-init", "--", "/run-sogo.sh" ]
USER 1001
