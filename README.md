# k8s SOGo

SOGo image.

Diverts from https://gitlab.com/synacksynack/opsperator/docker-apache

Depends on a Cyrus server, such as
https://gitlab.com/synacksynack/opsperator/docker-cyrus

Depends on an OpenLDAP server, such as
https://gitlab.com/synacksynack/opsperator/docker-openldap

Depends on a Postfix server, such as
https://gitlab.com/synacksynack/opsperator/docker-postfix

Build with:

```
$ make build
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name               |    Description                | Default                                                     | Inherited From    |
| :----------------------------- | ----------------------------- | ----------------------------------------------------------- | ----------------- |
|  `APACHE_DOMAIN`               | SoGO ServerName               | `mail.${OPENLDAP_DOMAIN}`                                   | opsperator/apache |
|  `APACHE_HTTP_PORT`            | SoGO HTTP(s) Port             | `8080`                                                      | opsperator/apache |
|  `API_HOST`                    | SoGO API Host                 | `127.0.0.1`                                                 |                   |
|  `IMAP_HOST`                   | SOGo IMAP Host                | `cyrus.demo.local`                                          |                   |
|  `IMAP_PORT`                   | SOGo IMAP Port                | `143`                                                       |                   |
|  `IMAP_PROTO`                  | SOGo IMAP Protocol            | `imap`                                                      |                   |
|  `MEMCACHED_HOST`              | Memcached Host                | undef                                                       |                   |
|  `MEMCACHED_PORT`              | Memcached Port                | `11211`                                                     |                   |
|  `MYSQL_DATABASE`              | MySQL Database Name           | `sogo`                                                      |                   |
|  `MYSQL_HOST`                  | MySQL Database Host           | `127.0.0.1`                                                 |                   |
|  `MYSQL_PASSWORD`              | MySQL Database Password       | undef                                                       |                   |
|  `MYSQL_PORT`                  | MySQL Database Port           | `3306`                                                      |                   |
|  `MYSQL_USER`                  | MySQL Database Username       | `sogo`                                                      |                   |
|  `ONLY_TRUST_KUBE_CA`          | Don't trust base image CAs    | `false`, any other value disables ca-certificates CAs       | opsperator/apache |
|  `OPENLDAP_BASE`               | OpenLDAP Base                 | seds `OPENLDAP_DOMAIN`, default produces `dc=demo,dc=local` | opsperator/apache |
|  `OPENLDAP_BIND_DN_RREFIX`     | OpenLDAP Bind DN Prefix       | `cn=sogo,ou=services`                                       | opsperator/apache |
|  `OPENLDAP_BIND_PW`            | OpenLDAP Bind Password        | `secret`                                                    | opsperator/apache |
|  `OPENLDAP_DOMAIN`             | OpenLDAP Domain Name          | `demo.local`                                                | opsperator/apache |
|  `OPENLDAP_HOST`               | OpenLDAP Backend Address      | `127.0.0.1`                                                 | opsperator/apache |
|  `OPENLDAP_PORT`               | OpenLDAP Bind Port            | `389` or `636` depending on `OPENLDAP_PROTO`                | opsperator/apache |
|  `OPENLDAP_PROTO`              | OpenLDAP Proto                | `ldap`                                                      | opsperator/apache |
|  `OPENLDAP_USERS_OBJECTCLASS`  | OpenLDAP Users ObjectClass    | `inetOrgPerson`                                             | opsperator/apache |
|  `POSTGRES_DB`                 | Postgres Database Name        | `sogo`                                                      |                   |
|  `POSTGRES_HOST`               | Postgres Database Host        | `127.0.0.1`                                                 |                   |
|  `POSTGRES_PASSWORD`           | Postgres Database Password    | undef                                                       |                   |
|  `POSTGRES_PORT`               | Postgres Database Port        | `5432`                                                      |                   |
|  `POSTGRES_USER`               | Postgres Database Username    | `sogo`                                                      |                   |
|  `PUBLIC_PROTO`                | Apache Public Proto           | `http`                                                      | opsperator/apache |
|  `SIEVE_HOST`                  | SOGo Sieve Relay              | `localhost`                                                 |                   |
|  `SIEVE_PORT`                  | SOGo Sieve Port               | `4190`                                                      |                   |
|  `SMTP_HOST`                   | SOGo SMTP Relay               | `localhost`                                                 |                   |
|  `SMTP_PORT`                   | SOGo SMTP Port                | `25`                                                        |                   |
|  `SMTP_PROTO`                  | SOGo SMTP Protocol            | `smtp`                                                      |                   |
|  `SOGO_LANG`                   | SOGo Language                 | `English`                                                   |                   |
|  `SOGO_MEM_LIMIT`              | SOGo Memory Limit             | `384`                                                       |                   |
|  `SOGO_SUPERUSERS`             | SOGo SuperUsers List          | `admin0`                                                    |                   |
|  `SOGO_TITLE`                  | SOGo Application Title        | `KubeSOGo`                                                  |                   |
|  `TZ`                          | SOGo Timezone                 | `UTC`                                                       |                   |
|  `WAIT_INTERVAL`               | SOGo Jobs Interval            | `300` (in seconds)                                          |                   |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point | Description                     | Inherited From    |
| :------------------ | ------------------------------- | ----------------- |
|  `/certs`           | Apache Certificate (optional)   | opsperator/apache |
