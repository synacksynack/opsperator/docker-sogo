#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

. /usr/local/bin/main-init.sh

while :
do
    if test -s /etc/sogo/sogo.conf; then
	break
    fi
    echo Waiting for SOGo configuration
    sleep 10
done

WAIT_INTERVAL=${WAIT_INTERVAL:-300}
while :
do
    started=$(date +%s)
    DAY_IS=$(date +%d)
    DO_DAILY=false
    if test -z "$DAY_WAS"; then
	DAY_WAS=$DAY_IS
	DO_DAILY=true
    elif ! test "$DAY_IS" = "$DAY_WAS"; then
	DAY_WAS=$DAY_IS
	DO_DAILY=true
    fi

    if $DO_DAILY; then
	echo "Pruning Spool From Older Messages & Empty Folders"
	find /var/spool/sogo -type f -atime +23 -exec rm -vf {} \;
	find /var/spool/sogo -mindepth 1 -type d -empty -exec rm -vrf {} \;
    fi

    echo Expiring Sessions
    /usr/sbin/sogo-tool expire-sessions 60

    if test "$DO_ALARMS"; then
	echo Sending Alarms If Necessary
	/usr/sbin/sogo-ealarms-notify
    fi

    stopped=$(date +%s)
    elapsed=`expr $stopped - $started`
    w=`expr $WAIT_INTERVAL - $elapsed`
    if test "$w" -lt 60; then
	w=$WAIT_INTERVAL
    fi
    sleep $w
done
