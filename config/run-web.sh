#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

. /usr/local/bin/main-init.sh
. /usr/local/bin/nsswrapper.sh

APACHE_DOMAIN=${APACHE_DOMAIN:-mail.demo.local}
APACHE_HTTP_PORT=${APACHE_HTTP_PORT:-8080}
API_HOST=${API_HOST:-127.0.0.1}
if ! ls /etc/apache2/sites-enabled/*.conf >/dev/null 2>&1; then
    echo "Generates SOGo VirtualHost Configuration"
    (
	cat <<EOF
<VirtualHost *:$APACHE_HTTP_PORT>
    ServerName $APACHE_DOMAIN
    CustomLog /dev/stdout modremoteip
    Include "/etc/apache2/$SSL_INCLUDE.conf"
    AddDefaultCharset UTF-8
    Alias /favicon.ico /usr/lib/GNUstep/SOGo/WebServerResources/img/sogo.ico
    Alias /SOGo.woa/WebServerResources/ /usr/lib/GNUstep/SOGo/WebServerResources/
    Alias /SOGo/WebServerResources/ /usr/lib/GNUstep/SOGo/WebServerResources/
    <Directory /usr/lib/GNUstep/SOGo/>
	AllowOverride None
	<IfVersion < 2.4>
	    Order deny,allow
	    Allow from all
	</IfVersion>
	<IfVersion >= 2.4>
	    Require all granted
	</IfVersion>
	<IfModule expires_module>
	  ExpiresActive On
	  ExpiresDefault "access plus 1 year"
	</IfModule>
    </Directory>
EOF
	if test "$AUTH_METHOD" = ldap; then
	    cat <<EOF
    <Location /SOGo>
	AuthType Basic
	AuthBasicProvider ldap
	AuthLDAPBindDN "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE
	AuthLDAPBindPassword "$OPENLDAP_BIND_PW"
	AuthLDAPURL "$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT/$OPENLDAP_SEARCH" NONE
	AuthName "LDAP Auth"
	AllowOverride None
	SetEnv proxy-nokeepalive 1
	Require valid-user
    </Location>
EOF
	fi
    cat <<EOF
    ProxyRequests Off
    SetEnv proxy-nokeepalive 1
    ProxyPreserveHost On

    RedirectMatch ^/$ $PUBLIC_PROTO://$APACHE_DOMAIN/SOGo
    ProxyPass /Microsoft-Server-ActiveSync http://$API_HOST:20000/SOGo/Microsoft-Server-ActiveSync retry=60 connectiontimeout=5 timeout=360
    ProxyPass /SOGo http://$API_HOST:20000/SOGo retry=0

    <Proxy http://$API_HOST:20000/SOGo>
	<IfModule headers_module>
	    SetEnvIf Host (.*) HTTP_HOST=\$1
	    RequestHeader set "x-webobjects-server-name" "%{HTTP_HOST}e" env=HTTP_HOST
	    RequestHeader set "x-webobjects-server-port" "443"
	    RequestHeader set "x-webobjects-server-protocol" "HTTP/1.0"
	    RequestHeader set "x-webobjects-server-url" "https://%{HTTP_HOST}e" env=HTTP_HOST

	    RequestHeader unset "x-webobjects-remote-user"
	    #when using proxy auth, replace with:
	    #RequestHeader set "x-webobjects-remote-user" "%{REMOTE_USER}e" env=REMOTE_USER
	</IfModule>
	Order allow,deny
	Allow from all
    </Proxy>

    <IfModule rewrite_module>
	RewriteEngine On
	RewriteRule ^/.well-known/caldav/?$ /SOGo/dav [R=301]
	RewriteRule ^/.well-known/carddav/?$ /SOGo/dav [R=301]
    </IfModule>
</VirtualHost>
EOF
    ) >/etc/apache2/sites-enabled/003-vhosts.conf
    if test "$AUTH_METHOD" = ldap; then
	export APACHE_IGNORE_OPENLDAP=yay
	if ! test -s /etc/ldap/ldap.conf; then
	    if test "$OPENLDAP_PROTO" = ldaps -a "$LDAP_SKIP_TLS_VERIFY"; then
		echo "LDAPVerifyServerCert Off" \
		    >>/etc/apache2/sites-enabled/003-vhosts.conf
		cat <<EOF
URI		$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT
TLS_REQCERT	never
EOF
	    elif test "$OPENLDAP_PROTO" = ldaps; then
		cat <<EOF
URI		$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT
TLS_REQCERT	demand
EOF
	    fi >/etc/ldap/ldap.conf
	fi
    fi
fi

export APACHE_DOMAIN
export APACHE_HTTP_PORT
export RESET_TLS=false

set -- /usr/sbin/apache2ctl -D FOREGROUND
. /run-apache.sh
