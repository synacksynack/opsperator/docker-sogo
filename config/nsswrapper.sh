#!/bin/sh

if test "`id -u`" -ne 0; then
    if test -s /tmp/sogo-passwd; then
	echo Skipping nsswrapper setup - already initialized
    else
	echo Setting up nsswrapper mapping `id -u` to sogo
	sed "s|^sogo:.*|sogo:x:`id -g`:|" /etc/group >/tmp/sogo-group
	sed \
	    "s|^sogo:.*|sogo:x:`id -u`:`id -g`:sogo:/var/lib/sogo:/usr/sbin/nologin|" \
	    /etc/passwd >/tmp/sogo-passwd
    fi
    export NSS_WRAPPER_PASSWD=/tmp/sogo-passwd
    export NSS_WRAPPER_GROUP=/tmp/sogo-group
    export LD_PRELOAD=/usr/lib/libnss_wrapper.so
fi
