#!/bin/sh

if test "$DEBUG"; then
    set -x
    SOGO_DEBUG=YES
else
    SOGO_DEBUG=NO
fi

. /usr/local/bin/main-init.sh

IMAP_PROTO=${IMAP_PROTO:-imap}
IMAP_HOST=${IMAP_HOST:-localhost}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=sogo,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
OPENLDAP_USERS_OBJECTCLASS=${OPENLDAP_USERS_OBJECTCLASS:-inetOrgPerson}
SIEVE_PORT=${SIEVE_PORT:-4190}
SIEVE_HOST=${SIEVE_HOST:-localhost}
SMTP_PROTO=${SMTP_PROTO:-smtp}
SMTP_HOST=${SMTP_HOST:-localhost}
SOGO_LANG=${SOGO_LANG:-English}
SOGO_MEM_LIMIT=${SOGO_MEM_LIMIT:-384}

if test "$POSTGRES_PASSWORD"; then
    DBURL="postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST:$POSTGRES_PORT/$POSTGRES_DB"
elif test "$MYSQL_PASSWORD"; then
    DBURL="mysql://$MYSQL_USER:$MYSQL_PASSWORD@$MYSQL_HOST:$MYSQL_PORT/$MYSQL_DATABASE"
fi
if test -z "$IMAP_PORT" -a "$IMAP_PROTO" = imap; then
    IMAP_PORT=143
elif test -z "$IMAP_PORT"; then
    IMAP_PORT=993
fi
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi
if test -z "$SMTP_PORT" -a "$SMTP_PROTO" = smtp; then
    SMTP_PORT=25
elif test -z "$SMTP_PORT"; then
    SMTP_PORT=465
fi
if test -z "$SOGO_SUPERUSERS"; then
    SOGO_SUPERUSERS="admin0 demoone"
fi
if test -z "$SOGO_TITLE"; then
    SOGO_TITLE="KubeSOGo"
fi
if test "$MEMACHED_HOST"; then
    MEMACHED_PORT=${MEMACHED_PORT:-11211}
    for h in $MEMCACHED_HOST
    do
	if test "$hlist"; then
	    hlist="$hlist,"
	fi
	if echo "$h" | grep : >/dev/null; then
	    hlist="$hlist $h"
	else
	    hlist="$hlist $h:$MEMCACHED_PORT"
	fi
    done
    MEMCACHED_TOGGLE=SOGoMemcachedHost
else
    MEMCACHED_TOGGLE=//SOGoMemcachedHost
    hlist=
fi

SOGO_SUPERUSERS=`echo "$SOGO_SUPERUSERS" | sed 's|\([^,]\) |\1, |g'`
sed -e "s|DBURL|$DBURL|" \
    -e "s|IMAP_HOST|$IMAP_HOST|" \
    -e "s|IMAP_PORT|$IMAP_PORT|" \
    -e "s|IMAP_PROTO|$IMAP_PROTO|" \
    -e "s|MEMCACHED_HOSTS|$hlist|" \
    -e "s|OPENLDAP_BASE|$OPENLDAP_BASE|" \
    -e "s|OPENLDAP_BIND_DN_PREFIX|$OPENLDAP_BIND_DN_PREFIX|" \
    -e "s|OPENLDAP_BIND_PW|$OPENLDAP_BIND_PW|" \
    -e "s|OPENLDAP_DOMAIN|$OPENLDAP_DOMAIN|" \
    -e "s|OPENLDAP_HOST|$OPENLDAP_HOST|" \
    -e "s|OPENLDAP_PORT|$OPENLDAP_PORT|" \
    -e "s|OPENLDAP_PROTO|$OPENLDAP_PROTO|" \
    -e "s|OPENLDAP_USERS_OBJECTCLASS|$OPENLDAP_USERS_OBJECTCLASS|" \
    -e "s|SIEVE_HOST|$SIEVE_HOST|" \
    -e "s|SIEVE_PORT|$SIEVE_PORT|" \
    -e "s|SMTP_HOST|$SMTP_HOST|" \
    -e "s|SMTP_PORT|$SMTP_PORT|" \
    -e "s|SMTP_PROTO|$SMTP_PROTO|" \
    -e "s|SOGO_DEBUG|$SOGO_DEBUG|" \
    -e "s|SOGO_LANG|$SOGO_LANG|" \
    -e "s|SOGO_MEM_LIMIT|$SOGO_MEM_LIMIT|" \
    -e "s|SOGO_TITLE|$SOGO_TITLE|" \
    -e "s|SOGO_SUPERUSERS|$SOGO_SUPERUSERS|" \
    -e "s|TZ|$TZ|" \
    -e "s|//SOGoMemcachedHost|$MEMCACHED_TOGGLE|" \
    /sogo.sample >/etc/sogo/sogo.conf

echo Tailing SOGo logs

mkdir -p /run/sogo /var/spool/sogo /var/log/sogo
if ! test -f /var/log/sogo/sogo.log; then
    touch /var/log/sogo/sogo.log
fi
tail -f /var/log/sogo/sogo.log &

echo Starting up SOGo

. /usr/share/GNUstep/Makefiles/GNUstep.sh
exec /usr/sbin/sogod -WONoDetach YES -WOPidFile /run/sogo/sogo.pid
