---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    backups.opsperator.io/driver: ssh
    backups.opsperator.io/interval: daily
    backups.opsperator.io/preferredtime: "15"
    backups.opsperator.io/secret: backups-cyrus-kube
    backups.opsperator.io/service: backups-cyrus-kube
    name: cyrus-kube
  name: cyrus-kube
  namespace: ci
spec:
  progressDeadlineSeconds: 600
  replicas: 1
  revisionHistoryLimit: 3
  selector:
    matchLabels:
      name: cyrus-kube
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        name: cyrus-kube
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: name
                operator: In
                values:
                - cyrus-kube
            topologyKey: kubernetes.io/hostname
      containers:
      - args:
        - /start-cyrus.sh
        env:
        - name: CYRUS_ADMIN
          value: cyrus
        - name: DEBUG
          value: yay
        - name: TZ
          value: Europe/Paris
        image: registry.gitlab.com/synacksynack/opsperator/docker-cyrus:master
        imagePullPolicy: Always
        livenessProbe:
          exec:
            command:
            - sh
            - -c
            - |
                if test -e /run/cyrus-master.pid; then
                    haspid=$(cat /run/cyrus-master.pid);
                    if test -e /proc/$haspid/cmdline; then
                        exit 0;
                    fi;
                fi;
                exit 1;
          failureThreshold: 10
          initialDelaySeconds: 30
          periodSeconds: 20
          successThreshold: 1
          timeoutSeconds: 2
        name: cyrus
        ports:
        - containerPort: 24
          protocol: TCP
        - containerPort: 110
          protocol: TCP
        - containerPort: 143
          protocol: TCP
        - containerPort: 993
          protocol: TCP
        - containerPort: 995
          protocol: TCP
        - containerPort: 4190
          protocol: TCP
        resources:
          limits:
            cpu: 300m
            memory: 1Gi
          requests:
            cpu: 100m
            memory: 256Mi
        securityContext:
          privileged: true
        volumeMounts:
        - mountPath: /certs
          name: pkiconf
        - mountPath: /etc/cyrus.conf.d/cyrus.conf
          name: config
          subPath: cyrus.conf
        - mountPath: /etc/imapd.conf.d/imapd.conf
          name: config
          subPath: imapd.conf
        - mountPath: /etc/openldap
          name: ldapconf
        - mountPath: /etc/pki/cyrus-imapd
          name: data
          subPath: pki
        - mountPath: /etc/saslauthd.conf.d/saslauthd.conf
          name: authconfig
          subPath: saslauthd.conf
        - mountPath: /run/rsyslog
          name: tmp
          subPath: rsyslog
        - mountPath: /run/saslauthd
          name: tmp
          subPath: saslauthd
        - mountPath: /var/lib/cyrus
          name: data
          subPath: lib
        - mountPath: /var/lib/imap
          name: data
          subPath: lib
        - mountPath: /var/spool/cyrus
          name: data
          subPath: spool
        - mountPath: /var/spool/imap
          name: data
          subPath: spool
      - args:
        - /start-syslog.sh
        env:
        - name: DEBUG
          value: yay
        - name: TZ
          value: Europe/Paris
        image: registry.gitlab.com/synacksynack/opsperator/docker-cyrus:master
        imagePullPolicy: Always
        livenessProbe:
          exec:
            command:
            - sh
            - -c
            - |
              test -e /run/rsyslog/dev/log || exit 1
          failureThreshold: 10
          initialDelaySeconds: 30
          periodSeconds: 20
          successThreshold: 1
          timeoutSeconds: 2
        name: syslog
        resources:
          limits:
            cpu: 100m
            memory: 512Mi
          requests:
            cpu: 25m
            memory: 256Mi
        securityContext:
          privileged: true
        volumeMounts:
        - mountPath: /etc/cyrus.conf
          name: config
          subPath: dummy.conf
        - mountPath: /etc/imapd.conf
          name: config
          subPath: dummy.conf
        - mountPath: /etc/saslauthd.conf
          name: config
          subPath: dummy.conf
        - mountPath: /run/rsyslog
          name: tmp
          subPath: rsyslog
      - args:
        - /start-saslauthd.sh
        env:
        - name: DEBUG
          value: yay
        - name: TZ
          value: Europe/Paris
        image: registry.gitlab.com/synacksynack/opsperator/docker-cyrus:master
        imagePullPolicy: Always
        livenessProbe:
          exec:
            command:
            - sh
            - -c
            - |
              test -e /run/saslauthd/mux || exit 1
          failureThreshold: 10
          initialDelaySeconds: 30
          periodSeconds: 20
          successThreshold: 1
          timeoutSeconds: 2
        name: saslauthd
        resources:
          limits:
            cpu: 100m
            memory: 512Mi
          requests:
            cpu: 25m
            memory: 256Mi
        securityContext:
          privileged: true
        volumeMounts:
        - mountPath: /certs
          name: pkiconf
        - mountPath: /etc/cyrus.conf
          name: config
          subPath: dummy.conf
        - mountPath: /etc/imapd.conf
          name: config
          subPath: dummy.conf
        - mountPath: /etc/openldap
          name: ldapconf
        - mountPath: /etc/saslauthd.conf.d/saslauthd.conf
          name: authconfig
          subPath: saslauthd.conf
        - mountPath: /run/rsyslog
          name: tmp
          subPath: rsyslog
        - mountPath: /run/saslauthd
          name: tmp
          subPath: saslauthd
      - args:
        - /run-job.sh
        env:
        - name: DEBUG
          value: yay
        - name: CYRUS_ADMIN
          value: cyrus
        - name: CYRUS_PASSWORD
          valueFrom:
            secretKeyRef:
              key: cyrus-password
              name: openldap-kube
        - name: OPENLDAP_BASE
          valueFrom:
            secretKeyRef:
              key: directory-root
              name: openldap-kube
        - name: OPENLDAP_BIND_DN_PREFIX
          value: cn=cyrus,ou=services
        - name: OPENLDAP_BIND_PW
          valueFrom:
            secretKeyRef:
              key: cyrus-password
              name: openldap-kube
        - name: OPENLDAP_DOMAIN
          value: ci.apps.intra.unetresgrossebite.com
        - name: OPENLDAP_HOST
          value: openldap-kube.ci.svc.cluster.local
        - name: OPENLDAP_PORT
          value: "1636"
        - name: OPENLDAP_PROTO
          value: ldaps
        - name: TZ
          value: Europe/Paris
        image: registry.gitlab.com/synacksynack/opsperator/docker-cyrus:master
        imagePullPolicy: Always
        name: job
        resources:
          limits:
            cpu: 100m
            memory: 768Mi
          requests:
            cpu: 10m
            memory: 128Mi
        securityContext:
          privileged: true
        volumeMounts:
        - mountPath: /certs
          name: pkiconf
        - mountPath: /etc/cyrus.conf
          name: config
          subPath: dummy.conf
        - mountPath: /etc/imapd.conf
          name: config
          subPath: dummy.conf
        - mountPath: /etc/openldap
          name: ldapconf
        - mountPath: /etc/saslauthd.conf
          name: authconfig
          subPath: dummy.conf
        - mountPath: /tmp
          name: tmp
          subPath: jobtemp
        - mountPath: /var/spool/imap
          name: data
          subPath: spool
      - image: registry.gitlab.com/synacksynack/opsperator/docker-sshd:master
        imagePullPolicy: Always
        livenessProbe:
          failureThreshold: 3
          initialDelaySeconds: 30
          periodSeconds: 10
          successThreshold: 1
          tcpSocket:
            port: 2222
          timeoutSeconds: 3
        name: sshd
        ports:
        - containerPort: 2222
          protocol: TCP
        readinessProbe:
          failureThreshold: 3
          initialDelaySeconds: 30
          periodSeconds: 10
          successThreshold: 1
          tcpSocket:
            port: 2222
          timeoutSeconds: 1
        resources:
          limits:
            cpu: 100m
            memory: 512Mi
          requests:
            cpu: 10m
            memory: 128Mi
        volumeMounts:
        - mountPath: /.ssh/id_rsa.pub
          name: ssh
          subPath: id_rsa.pub
        - mountPath: /backups
          name: data
      serviceAccount: cyrus-kube
      serviceAccountName: cyrus-kube
      terminationGracePeriodSeconds: 600
      volumes:
      - name: authconfig
        secret:
          defaultMode: 420
          secretName: cyrus-kube
      - name: data
        persistentVolumeClaim:
          claimName: cyrus-kube
      - configMap:
          defaultMode: 420
          name: cyrus-kube
        name: config
      - configMap:
          defaultMode: 420
          name: openldap-kube-config
        name: ldapconf
      - name: pkiconf
        projected:
          defaultMode: 420
          sources:
          - secret:
              items:
              - key: ca.crt
                path: ca.crt
              name: kube-root-ca
          - secret:
              items:
              - key: tls.crt
                path: tls.crt
              name: cyrus-kube-tls
          - secret:
              items:
              - key: tls.key
                path: tls.key
              name: cyrus-kube-tls
      - name: ssh
        secret:
          defaultMode: 420
          secretName: backups-cyrus-kube
      - emptyDir: {}
        name: tmp
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    name: sogo-kube
  name: sogo-kube
  namespace: ci
spec:
  progressDeadlineSeconds: 600
  replicas: 1
  revisionHistoryLimit: 3
  selector:
    matchLabels:
      name: sogo-kube
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      labels:
        name: sogo-kube
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: name
                operator: In
                values:
                - sogo-kube
            topologyKey: kubernetes.io/hostname
      containers:
      - command:
        - dumb-init
        - --
        - /run-sogo.sh
        env:
        - name: APACHE_DOMAIN
          value: sogo.ci.apps.intra.unetresgrossebite.com
        - name: APACHE_HTTP_PORT
          value: "8080"
        - name: DEBUG
        - name: IMAP_HOST
          value: cyrus-kube.ci.svc.cluster.local
        - name: IMAP_PORT
          value: "993"
        - name: IMAP_PROTO
          value: imaps
        - name: MEMCACHED_HOST
        - name: MYSQL_DATABASE
          valueFrom:
            secretKeyRef:
              key: database-name
              name: sogo-mariadb-kube
        - name: MYSQL_HOST
          value: sogo-mariadb-kube
        - name: MYSQL_PASSWORD
          valueFrom:
            secretKeyRef:
              key: database-password
              name: sogo-mariadb-kube
        - name: MYSQL_USER
          valueFrom:
            secretKeyRef:
              key: database-user
              name: sogo-mariadb-kube
        - name: ONLY_TRUST_KUBE_CA
          value: "true"
        - name: OPENLDAP_BASE
          valueFrom:
            secretKeyRef:
              key: directory-root
              name: openldap-kube
        - name: OPENLDAP_BIND_DN_PREFIX
          value: cn=sogo,ou=services
        - name: OPENLDAP_BIND_PW
          valueFrom:
            secretKeyRef:
              key: sogo-password
              name: openldap-kube
        - name: OPENLDAP_DOMAIN
          value: ci.apps.intra.unetresgrossebite.com
        - name: OPENLDAP_HOST
          value: openldap-kube.ci.svc.cluster.local
        - name: OPENLDAP_PORT
          value: "1636"
        - name: OPENLDAP_PROTO
          value: ldaps
        - name: OPENLDAP_USERS_OBJECTCLASS
          value: wsweetUser
        - name: POSTGRES_PASSWORD
        - name: PUBLIC_PROTO
          value: https
        - name: SIEVE_HOST
          value: cyrus-kube-internal.ci.svc.cluster.local
        - name: SIEVE_PORT
          value: "4190"
        - name: SMTP_HOST
          value: postfix-kube.ci.svc.cluster.local
        - name: SMTP_PORT
          value: "465"
        - name: SMTP_PROTO
          value: smtps
        - name: SOGO_LANG
          value: English
        - name: SOGO_MEM_LIMIT
          value: "384"
        - name: SOGO_MEMLIMIT
          value: "384"
        - name: SOGO_SUPERUSERS
          value: admin0
        - name: SOGO_TITLE
          value: KubeSOGo
        - name: TZ
          value: Europe/Paris
        image: registry.gitlab.com/synacksynack/opsperator/docker-sogo:master
        imagePullPolicy: Always
        livenessProbe:
          exec:
            command:
            - sh
            - -c
            - |
              test -s /var/run/sogo/sogo.pid || exit 1
          failureThreshold: 28
          initialDelaySeconds: 60
          periodSeconds: 30
          successThreshold: 1
          timeoutSeconds: 5
        name: sogo
        ports:
        - containerPort: 20000
          protocol: TCP
        resources:
          limits:
            cpu: 200m
            memory: 768Mi
          requests:
            cpu: 50m
            memory: 512Mi
        volumeMounts:
        - mountPath: /certs
          name: pkiconf
        - mountPath: /etc/ldap
          name: ldapconf
        - mountPath: /etc/sogo
          name: temp
          subPath: config
      - command:
        - dumb-init
        - --
        - /run-web.sh
        env:
        - name: APACHE_DOMAIN
          value: sogo.ci.apps.intra.unetresgrossebite.com
        - name: APACHE_HTTP_PORT
          value: "8080"
        - name: DEBUG
        - name: PUBLIC_PROTO
          value: https
        - name: TZ
          value: Europe/Paris
        image: registry.gitlab.com/synacksynack/opsperator/docker-sogo:master
        imagePullPolicy: Always
        livenessProbe:
          failureThreshold: 28
          initialDelaySeconds: 60
          periodSeconds: 30
          successThreshold: 1
          tcpSocket:
            port: 8080
          timeoutSeconds: 5
        name: proxy
        ports:
        - containerPort: 8080
          protocol: TCP
        readinessProbe:
          failureThreshold: 3
          httpGet:
            httpHeaders:
            - name: Host
              value: stats
            path: /
            port: 8080
            scheme: HTTP
          initialDelaySeconds: 30
          periodSeconds: 15
          successThreshold: 1
          timeoutSeconds: 3
        resources:
          limits:
            cpu: 200m
            memory: 768Mi
          requests:
            cpu: 10m
            memory: 128Mi
        volumeMounts:
        - mountPath: /certs
          name: pkiconf
        - mountPath: /etc/apache2/sites-enabled
          name: temp
          subPath: apachesites
        - mountPath: /etc/ldap
          name: ldapconf
      - command:
        - dumb-init
        - --
        - /run-job.sh
        env:
        - name: DEBUG
        - name: ONLY_TRUST_KUBE_CA
          value: "true"
        - name: TZ
          value: Europe/Paris
        image: registry.gitlab.com/synacksynack/opsperator/docker-sogo:master
        imagePullPolicy: Always
        name: job
        resources:
          limits:
            cpu: 50m
            memory: 512Mi
          requests:
            cpu: 10m
            memory: 64Mi
        volumeMounts:
        - mountPath: /certs
          name: pkiconf
        - mountPath: /etc/ldap
          name: ldapconf
        - mountPath: /etc/sogo
          name: temp
          subPath: config
      - args:
        - --scrape_uri
        - http://stats:8080/server-status
        - --telemetry.address=:9113
        image: registry.gitlab.com/synacksynack/opsperator/docker-httpexporter:master
        imagePullPolicy: Always
        livenessProbe:
          failureThreshold: 38
          httpGet:
            path: /
            port: 9113
            scheme: HTTP
          initialDelaySeconds: 60
          periodSeconds: 30
          successThreshold: 1
          timeoutSeconds: 3
        name: exporter
        ports:
        - containerPort: 9113
          protocol: TCP
        readinessProbe:
          failureThreshold: 3
          httpGet:
            path: /
            port: 9113
            scheme: HTTP
          initialDelaySeconds: 30
          periodSeconds: 15
          successThreshold: 1
          timeoutSeconds: 3
        resources:
          limits:
            cpu: 100m
            memory: 128Mi
          requests:
            cpu: 10m
            memory: 64Mi
      hostAliases:
      - hostnames:
        - stats
        ip: 127.0.0.1
      volumes:
      - emptyDir: {}
        name: temp
      - configMap:
          defaultMode: 420
          name: openldap-kube-config
        name: ldapconf
      - name: pkiconf
        projected:
          defaultMode: 420
          sources:
          - secret:
              items:
              - key: ca.crt
                path: ca.crt
              name: kube-root-ca
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    backups.opsperator.io/driver: mysql
    backups.opsperator.io/interval: daily
    backups.opsperator.io/preferredtime: "15"
    backups.opsperator.io/secret: sogo-mariadb-kube
    name: sogo-mariadb-kube
  name: sogo-mariadb-kube
  namespace: ci
spec:
  progressDeadlineSeconds: 600
  replicas: 1
  revisionHistoryLimit: 3
  selector:
    matchLabels:
      name: sogo-mariadb-kube
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        name: sogo-mariadb-kube
    spec:
      containers:
      - env:
        - name: DEBUG
          value: yay
        - name: MYSQL_DATABASE
          valueFrom:
            secretKeyRef:
              key: database-name
              name: sogo-mariadb-kube
        - name: MYSQL_PASSWORD
          valueFrom:
            secretKeyRef:
              key: database-password
              name: sogo-mariadb-kube
        - name: MYSQL_ROOT_PASSWORD
          valueFrom:
            secretKeyRef:
              key: database-admin-password
              name: sogo-mariadb-kube
        - name: MYSQL_USER
          valueFrom:
            secretKeyRef:
              key: database-user
              name: sogo-mariadb-kube
        - name: TZ
          value: Europe/Paris
        image: docker.io/centos/mariadb-103-centos7:latest
        imagePullPolicy: Always
        livenessProbe:
          failureThreshold: 38
          initialDelaySeconds: 30
          periodSeconds: 15
          successThreshold: 1
          tcpSocket:
            port: 3306
          timeoutSeconds: 3
        name: mariadb
        ports:
        - containerPort: 3306
          protocol: TCP
        readinessProbe:
          exec:
            command:
            - /bin/sh
            - -i
            - -c
            - MYSQL_PWD="$MYSQL_PASSWORD" mysql -h 127.0.0.1 -u $MYSQL_USER -D $MYSQL_DATABASE
              -e 'SELECT 1'
          failureThreshold: 6
          initialDelaySeconds: 5
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 3
        resources:
          limits:
            cpu: 250m
            memory: 512Mi
          requests:
            cpu: 250m
            memory: 512Mi
        volumeMounts:
        - mountPath: /var/lib/mysql/data
          name: db
      - args:
        - --collect.auto_increment.columns
        - --collect.binlog_size
        - --collect.engine_innodb_status
        - --collect.global_status
        - --collect.global_variables
        - --collect.info_schema.processlist
        - --collect.info_schema.userstats
        - --collect.info_schema.innodb_cmp
        - --collect.info_schema.innodb_cmpmem
        - --collect.info_schema.innodb_metrics
        - --collect.info_schema.innodb_tablespaces
        - --collect.info_schema.query_response_time
        - --collect.info_schema.tables
        - --collect.info_schema.tables.databases='*'
        - --collect.info_schema.tablestats
        - --collect.perf_schema.eventsstatements
        - --collect.perf_schema.eventswaits
        - --collect.perf_schema.file_events
        - --collect.perf_schema.file_instances
        - --collect.perf_schema.indexiowaits
        - --collect.perf_schema.tableiowaits
        - --collect.perf_schema.tablelocks
        env:
        - name: DATA_SOURCE_HOST
          value: sogo-mariadb-kube
        - name: DATA_SOURCE_PASS
          valueFrom:
            secretKeyRef:
              key: database-admin-password
              name: sogo-mariadb-kube
        - name: DATA_SOURCE_USER
          value: root
        - name: DEBUG
          value: yay
        image: registry.gitlab.com/synacksynack/opsperator/docker-mysqlexporter:master
        imagePullPolicy: Always
        livenessProbe:
          failureThreshold: 38
          initialDelaySeconds: 30
          periodSeconds: 15
          successThreshold: 1
          tcpSocket:
            port: 9113
          timeoutSeconds: 3
        name: exporter
        ports:
        - containerPort: 9113
          protocol: TCP
        readinessProbe:
          failureThreshold: 3
          httpGet:
            path: /
            port: 9113
            scheme: HTTP
          initialDelaySeconds: 5
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 3
        resources:
          limits:
            cpu: 100m
            memory: 128Mi
          requests:
            cpu: 100m
            memory: 128Mi
      terminationGracePeriodSeconds: 600
      volumes:
      - emptyDir: {}
        name: db
